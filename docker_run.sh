docker run -ti -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY -v $PWD:/home/repo/pynqc --device=/dev/usb/legousbtower0 pynqc:latest bash || \
  echo -e "Starting docker without mounting a device"; \
  docker run -ti -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY -v $PWD:/home/repo/pynqc pynqc:latest bash
