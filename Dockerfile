ARG BASE_IMAGE=SHOULD_BE_OVERRIDDEN
ARG TAG=latest

FROM $BASE_IMAGE:$TAG
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update --fix-missing -y; \
    apt-get -qq upgrade -y;

RUN apt-get install -y -qq \
    python3.7 \
    python3-pip \
    wget \
    bison \
    flex \
    vim
 
RUN useradd -rm -d /home/repo -s /bin/bash -g root -G sudo -u 1000 ubuntu
# RUN echo 'ubuntu:ubuntu' | chpasswd
WORKDIR /home/repo

RUN mkdir -p /home/repo/nqc-3.1.r6 && \
    cd /home/repo/nqc-3.1.r6 && \
    wget http://bricxcc.sourceforge.net/nqc/release/nqc-3.1.r6.tgz && \
    tar xfz nqc-3.1.r6.tgz
RUN cd /home/repo/ && \
    wget http://sourceforge.net/p/bricxcc/patches/_discuss/thread/00b427dc/b84b/attachment/nqc-01-Linux_usb_and_tcp.diff && \
    patch -p0 < nqc-01-Linux_usb_and_tcp.diff 
RUN cd /home/repo/nqc-3.1.r6 && \
    make clean && \
    make DEFAULT_USB_NAME='"/dev/usb/legousbtower0"' && \
    chmod 644 nqc-man-2.1r1-0.man && \
    make install

RUN pip3 install \
    tox 

RUN apt-get install -y wish
ENV DISPLAY :0
USER ubuntu
