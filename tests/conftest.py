import pytest
import subprocess
import socket
import time
from contextlib import closing


def find_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(("", 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


def pytest_configure():
    pytest.emu = None
    pytest.emu_server = None
    pytest.ir_server = None

    try:
        pytest.ir_server = subprocess.Popen(
            ["./tests/ir-server"], stdout=subprocess.PIPE
        )
        port = find_free_port()
        pytest.emu_server = subprocess.Popen(
            [
                "python3",
                "./tests/emu_server.py",
                f"{port}",
                "./firmware/firm0309.lgo",
            ],
            stdout=subprocess.PIPE,
        )
        time.sleep(1)
        pytest.emu = subprocess.Popen(
            ["./tests/emu", "-serverport", f"{port}"], stdout=subprocess.PIPE
        )
    except subprocess.CalledProcessError as e:
        if emu:
            emu.kill
        if emu_server:
            emu_server.kill
        if ir_server:
            ir_server.kill


def _check_ir_tower_connected(rf_obj):
    global i
    ir_tower_connected = False
    try:
        _ = rf_obj.is_alive()

    except IOError as err:
        if err.errno == 255:
            ir_tower_connected = False
    else:
        ir_tower_connected = True
    return ir_tower_connected


def _check_dut_available(rf_obj):
    dut_available = False
    response = rf_obj.is_alive()
    if not response:
        dut_available = False
    else:
        dut_available = True
    return dut_available


def _prepare_dut(rf_obj):
    exp_version = 3.9
    response = rf_obj.get_versions()
    if exp_version != float(response["desc-ram"]):
        path = "./firmware/firm0309.lgo"
        rf_obj.nqc.flash_firmware(path)


def is_dut_available(rf_obj):
    dut_available = False
    ir_tower_connected = _check_ir_tower_connected(rf_obj)
    if ir_tower_connected:
        dut_available = _check_dut_available(rf_obj)
        if dut_available:
            _prepare_dut(rf_obj)
    return dut_available


@pytest.fixture(scope="session", autouse=True)
def setup_emu(request):

    request.addfinalizer(pytest.ir_server.kill)
    request.addfinalizer(pytest.emu_server.kill)
    request.addfinalizer(pytest.emu.kill)
