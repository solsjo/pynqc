import socket
import sys
import time


def run_server(port, firmware_path):
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_address = ("localhost", port)
    print(f"starting up on port {server_address}")
    sock.bind(server_address)

    # Listen for incoming connections
    sock.listen(1)

    started = False
    init = False

    while True:
        # Wait for a connection
        print("waiting for a connection")
        connection, client_address = sock.accept()

        try:
            print("connection from")

            # Receive the data in small chunks and retransmit it
            while True:
                if not started:
                    connection.send(b"OO\r\n")
                data = connection.recv(16)
                if data and not started:
                    print(f"startup data received: {data}")
                    if b"L0,00\n" in data:
                        # startup
                        # L, update lcd
                        # A, update sensors
                        # M, update motors
                        time.sleep(0.2)
                        started = True
                if started and not init:
                    print("set sensor values")
                    connection.send(b"A23ff\r\n")
                    time.sleep(0.1)
                    connection.send(b"A13ff\r\n")
                    time.sleep(0.1)
                    connection.send(b"A03ff\r\n")
                    time.sleep(0.1)
                    connection.send(b"A3140\r\n")
                    time.sleep(0.1)
                    print("download firmware")
                    time.sleep(0.2)
                    connection.send(b"PR\r\n")
                    time.sleep(0.1)
                    connection.send(b"BO1\r\n")
                    time.sleep(0.1)
                    connection.send(b"BO0\r\n")
                    time.sleep(0.1)
                    connection.send(
                        b"F" + bytes(firmware_path.encode("ascii", "ignore")) + b"\n\n"
                    )
                    # connection.send(b"F/home/repo/pynqc/firmware/firm0309.lgo\n\n")
                    time.sleep(0.1)
                    connection.send(b"OO\r\n")
                    init = True

                if data and started:
                    print(f"received data: {data}")

        # after 100 "send_cmd {BO1}; after 100 {send_cmd {BO0}; after 100 {send_cm    d {F$filename}; send_cmd {OO}}}"

        finally:
            # Clean up the connection
            connection.close()


if __name__ == "__main__":
    run_server(int(sys.argv[1]), sys.argv[2])
