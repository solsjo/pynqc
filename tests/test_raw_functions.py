"""Raw Functions tests"""

from unittest import mock
import pynqc.raw_functions as _rf
import pynqc as _nqc
import pytest
from conftest import is_dut_available

rf = _rf.RawFunctions(_nqc.Nqc(serial_type="usb", device="/dev/usb/legousbtower0"))
emu_rf = _rf.RawFunctions(_nqc.Nqc(serial_type="tcp", device="127.0.0.1:50637"))


class TestRange:
    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_raw_command_construction(self):
        """Test raw command construction"""

        data = _rf._verify_and_build_cmd(
            "set_motor_power", source=_rf.IMMEDIATE, argument=7, motors=_rf.MOTOR_A
        )
        assert data == "13010207"

    def test__verify_range_continuous(self):
        with pytest.raises(ValueError, match=r"argument 'a_name'.*"):
            _rf._verify_range(_rf.BYTE_RNG, pow(2, _rf.BYTE_BIT_SIZE), "a_name")
        with pytest.raises(ValueError, match=r"argument 'a_name'.*"):
            _rf._verify_range(_rf.BYTE_RNG, -1, "a_name")

    def test__verify_range_discrete(self):
        with pytest.raises(ValueError, match=r"argument 'a_name'.*"):
            _rf._verify_range([0, 2], 3, "a_name")
        with pytest.raises(ValueError, match=r"argument 'a_name'.*"):
            _rf._verify_range([0, 2], 1, "a_name")

    def test_source_range_check(self):
        """Test source range check"""

        with pytest.raises(ValueError, match=r"source '16' not valid.*"):
            _ = _rf._verify_and_build_cmd(
                "set_motor_power", source=16, argument=7, motors=_rf.MOTOR_A
            )

    def test_argument_range_check(self):
        """Test argument range check"""

        with pytest.raises(ValueError, match=r"argument 'argument'.*"):
            _ = _rf._verify_and_build_cmd(
                "set_motor_power",
                source=_rf.IMMEDIATE,
                argument=100000,
                motors=_rf.MOTOR_A,
            )

    def test_other_range_check(self):
        """Test discrete range check"""

        with pytest.raises(ValueError, match=r"argument 'motors'.*"):
            _ = _rf._verify_and_build_cmd(
                "set_motor_power", source=_rf.IMMEDIATE, argument=7, motors=0
            )


class TestRawFunctions:
    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def raise_error_no_response(cmd):
        raise IOError(253, "No response from RCX")

    def raise_error_other(cmd):
        raise IOError(1, "other")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_is_alive_true(self):
        response = rf.is_alive()
        assert response
        rf.nqc.exec_raw.assert_called_with("10")

    @mock.patch.object(
        _nqc.Nqc, "exec_raw", mock.Mock(side_effect=raise_error_no_response)
    )
    def test_is_alive_false(self):
        response = rf.is_alive()
        assert not response

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=raise_error_other))
    def test_is_alive_fail(self):
        with pytest.raises(IOError, match=r"other"):
            _ = rf.is_alive()

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b"a726"]))
    def test_get_battery_power(self):
        response = rf.get_battery_power()
        assert response == 9895
        rf.nqc.exec_raw.assert_called_with("30")

    def test_get_memory_map(self):
        with pytest.raises(NotImplementedError):
            _ = rf.get_memory_map()

    @mock.patch.object(
        _nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b"0003000100030009"])
    )
    def test_get_versions(self):
        response = rf.get_versions()
        assert response == {
            "rom-firmware": "03000100",
            "ram-firmware": "03000900",
            "desc-rom": "3.1",
            "desc-ram": "3.9",
        }
        rf.nqc.exec_raw.assert_called_with("15010305070b")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_power_off(self):
        response = rf.power_off()
        assert response
        rf.nqc.exec_raw.assert_called_with("60")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_set_power_down_delay(self):
        response = rf.set_power_down_delay(0)
        assert response
        rf.nqc.exec_raw.assert_called_with("b100")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_send_message(self):
        response = rf.send_message(_rf.IMMEDIATE, 0)
        assert response
        rf.nqc.exec_raw.assert_called_with("b20200")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_set_message(self):
        response = rf.set_message(0)
        assert response
        rf.nqc.exec_raw.assert_called_with("f700")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_set_display(self):
        response = rf.set_display(_rf.IMMEDIATE, 1)
        assert response
        rf.nqc.exec_raw.assert_called_with("33020100")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b"00 \n"]))
    def test_set_datalog_size(self):
        response = rf.set_datalog_size(90)
        assert response
        rf.nqc.exec_raw.assert_called_with("525a00")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b"00 \n"]))
    def test_datalog_next(self):
        response = rf.datalog_next(_rf.VARIABLE, 31)
        assert response
        rf.nqc.exec_raw.assert_called_with("62001f")

    @mock.patch.object(
        _nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b"ff03001d0100800420"])
    )
    def test_get_datalog(self):
        expected_log = [
            {
                "source_type": None,
                "source_index": 0,
                "source_desc": "Current datalog size",
                "value": 3,
            },
            {
                "source_type": 0,
                "source_index": 29,
                "source_desc": "Variable",
                "value": 1,
            },
            {
                "source_type": 14,
                "source_index": 0,
                "source_desc": "Clock",
                "value": 8196,
            },
        ]

        response = rf.get_datalog(0, 3)
        assert response == expected_log
        rf.nqc.exec_raw.assert_called_with("a400000300")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b"ff00"]))
    def test_get_value(self):
        response = rf.get_value(_rf.VARIABLE, 1)
        assert response == 255
        rf.nqc.exec_raw.assert_called_with("120001")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_set_variable(self):
        response = rf.set_variable(31, _rf.IMMEDIATE, 1)
        assert response
        rf.nqc.exec_raw.assert_called_with("141f020100")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_play_sound(self):
        response = rf.play_sound(0)
        assert response
        rf.nqc.exec_raw.assert_called_with("5100")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_play_tone(self):
        response = rf.play_tone(660, 200)
        assert response
        rf.nqc.exec_raw.assert_called_with("239402c8")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_set_motor_direction(self):
        response = rf.set_motor_direction(_rf.MOTOR_A, _rf.MOTOR_DIR_FWD)
        assert response
        rf.nqc.exec_raw.assert_called_with("e181")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_set_motor_state(self):
        response = rf.set_motor_state(_rf.MOTOR_A, _rf.MOTOR_ON)
        assert response
        rf.nqc.exec_raw.assert_called_with("2181")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_set_motor_power(self):
        response = rf.set_motor_power(_rf.MOTOR_A, _rf.IMMEDIATE, 0)
        assert response
        rf.nqc.exec_raw.assert_called_with("13010200")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_wait(self):
        response = rf.wait(_rf.IMMEDIATE, 200)
        assert response
        rf.nqc.exec_raw.assert_called_with("4302c800")

    @mock.patch.object(_nqc.Nqc, "exec_raw", mock.Mock(side_effect=[b""]))
    def test_set_time(self):
        response = rf.set_time(23, 59)
        assert response
        rf.nqc.exec_raw.assert_called_with("22173b")


class DUTBase:
    def get_rf(self):
        return rf

    def test_battery_readout(self):
        response = self.get_rf().get_battery_power()
        assert response > 8000

    def test_get_versions(self):
        response = self.get_rf().get_versions()
        assert response["desc-rom"] == "3.1"

    def test_set_message(self):
        loop_back_value = 253
        response = self.get_rf().set_message(loop_back_value)
        if not response:
            assert False, "set_message failed"
        response = self.get_rf().get_value(_rf.MESSAGE, 0)
        assert response == loop_back_value

    def test_set_variable(self):
        loop_back_value = 252
        response = self.get_rf().set_variable(1, _rf.IMMEDIATE, loop_back_value)
        if not response:
            assert False, "set_variable failed"
        response = self.get_rf().get_value(_rf.VARIABLE, 1)
        assert response == loop_back_value

    def test_motor_direction_readout(self):
        forward = 0x08
        reverse = 0
        motor_a = 0
        direction_bit = 0x08
        expected_direction = None
        cmd = None
        response = self.get_rf().get_value(_rf.MOTOR_STATE, motor_a)
        direction = response & direction_bit

        if direction == forward:
            expected_direction = reverse
            cmd = _rf.MOTOR_DIR_REV
        elif direction == reverse:
            expected_direction = forward
            cmd = _rf.MOTOR_DIR_FWD
        else:
            assert False, f"unknown state {direction}"

        response = self.get_rf().set_motor_direction(_rf.MOTOR_A, cmd)
        if not response:
            assert False, "set_motor_direction failed"

        response = self.get_rf().get_value(_rf.MOTOR_STATE, motor_a)
        new_direction = response & direction_bit

        assert (
            new_direction == expected_direction
        ), f"states: dir:{direction} exp_dir:{expected_direction} cmd: {cmd}"

    def test_motor_state_readout(self):
        on = 0x80
        off = 0x40
        floating = 0x00
        motor_b = 1
        expected_state = None
        response = self.get_rf().get_value(_rf.MOTOR_STATE, motor_b)
        state = response & (on | off)
        if state == on:
            expected_state = _rf.MOTOR_OFF
        elif state == off:
            expected_state = _rf.MOTOR_FLOAT
        elif state == floating:
            expected_state = _rf.MOTOR_OFF
        else:
            assert False, "unknown state"

        response = self.get_rf().set_motor_state(_rf.MOTOR_B, expected_state)
        if not response:
            assert False, "set_motor_state failed"

        response = self.get_rf().get_value(_rf.MOTOR_STATE, motor_b)
        new_state = response & (on | off)

        assert new_state == expected_state

    def test_datalog_readout(self):
        zero_exp = 111
        first_exp = 112
        second_exp = 113

        response = self.get_rf().set_variable(0, _rf.IMMEDIATE, zero_exp)
        if not response:
            assert False, "set variable failed"
        response = self.get_rf().set_variable(1, _rf.IMMEDIATE, first_exp)
        if not response:
            assert False, "set variable failed"
        response = self.get_rf().set_variable(2, _rf.IMMEDIATE, second_exp)
        if not response:
            assert False, "set variable failed"

        response = self.get_rf().set_datalog_size(3)
        if not response:
            assert False, "set datalog size failed"

        response = self.get_rf().datalog_next(_rf.VARIABLE, 0)
        if not response:
            assert False, "set datalog entry failed"
        response = self.get_rf().datalog_next(_rf.VARIABLE, 1)
        if not response:
            assert False, "set datalog entry failed"
        response = self.get_rf().datalog_next(_rf.VARIABLE, 2)
        if not response:
            assert False, "set datalog entry failed"

        response = self.get_rf().get_datalog(0, 1)
        if not response[0]["source_desc"] == "Current datalog size":
            assert (
                False
            ), f"first data log entry was not current datalog size {response}"
        response = self.get_rf().get_datalog(1, 3)
        assert (
            (response[0]["value"] == zero_exp)
            and (response[1]["value"] == first_exp)
            and (response[2]["value"] == second_exp)
        )

    def test_datalog_readout_out_of_range(self):
        datalog_size = 3

        response = self.get_rf().set_datalog_size(datalog_size)
        response = self.get_rf().get_datalog(1, datalog_size)
        if not len(response) == 3:
            assert len(response) == 3

        response = self.get_rf().get_datalog(1, datalog_size + 1)
        assert len(response) == 0


@pytest.mark.skipif(not is_dut_available(rf), reason="HW not available")
class TestOnHwDUT(DUTBase):
    def get_rf(self):
        return rf


@pytest.mark.skipif(
    not pytest.emu or not is_dut_available(emu_rf), reason="EMU not available"
)
class TestOnEmuDUT(DUTBase):
    def get_rf(self):
        return emu_rf

    @pytest.mark.skipif(
        True, reason="EMU does not seem to report correct firmware version"
    )
    def test_get_versions(self):
        pass

    @pytest.mark.skipif(True, reason="EMU does not seem to support datalog readout")
    def test_datalog_readout(self):
        pass

    @pytest.mark.skipif(True, reason="EMU does not seem to support datalog readout")
    def test_datalog_readout_out_of_range(self):
        pass
