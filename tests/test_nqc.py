"""NQC tests"""

from unittest import mock
import pynqc as _nqc
import pynqc.raw_functions as _rf
import pytest
from conftest import is_dut_available

nqc = _nqc.Nqc(serial_type="usb", device="/dev/usb/legousbtower0")
emu_nqc = _nqc.Nqc(serial_type="tcp", device="127.0.0.1:50637")
rf = _rf.RawFunctions(nqc)
emu_rf = _rf.RawFunctions(emu_nqc)


class TestNQC:
    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    @mock.patch("pynqc.subprocess.check_output", mock.Mock(side_effect=[b""]))
    def test_flash_and_exec_application(self):
        EXP_ID = 253
        EXP_VERSION = 255
        some_path = "./path/to/program"
        flags = ["-DOVERRIDE", f"-DID={EXP_ID}", f"-DVERSION={EXP_VERSION}"]
        response = nqc.flash_and_exec_application(some_path, flags)
        if not response:
            assert response

        exp_args = [
            "nqc",
            "-Susb:/dev/usb/legousbtower0",
            "-DOVERRIDE",
            "-DID=253",
            "-DVERSION=255",
            "-d",
            "./path/to/program",
            "-run",
        ]

        _nqc.subprocess.check_output.assert_called_with(exp_args, stderr=-1)

    @mock.patch("pynqc.subprocess.check_output", mock.Mock(side_effect=[b""]))
    def test_flash_firmware(self):
        some_path = "./path/to/firmware"
        response = nqc.flash_firmware(some_path)
        if not response:
            assert response

        exp_args = [
            "nqc",
            "-Susb:/dev/usb/legousbtower0",
            "-firmware",
            "./path/to/firmware",
        ]

        _nqc.subprocess.check_output.assert_called_with(exp_args, stderr=-1)

    @mock.patch("pynqc.subprocess.check_output", mock.Mock(side_effect=[b""]))
    def test_send_msg(self):
        response = nqc.send_msg(233)
        if not response:
            assert response

        exp_args = ["nqc", "-Susb:/dev/usb/legousbtower0", "-msg", "233"]

        _nqc.subprocess.check_output.assert_called_with(exp_args, stderr=-1)

    @mock.patch("pynqc.subprocess.check_output", mock.Mock(side_effect=[b""]))
    def test_set_program_slot(self):
        response = nqc.set_program_slot(1)
        if not response:
            assert response

        exp_args = ["nqc", "-Susb:/dev/usb/legousbtower0", "-pgm", "1"]

        _nqc.subprocess.check_output.assert_called_with(exp_args, stderr=-1)

    def test_set_program_slot_out_of_range(self):
        with pytest.raises(ValueError, match=r"slot.*"):
            _ = nqc.set_program_slot(6)

    @mock.patch("pynqc.subprocess.check_output", mock.Mock(side_effect=[b""]))
    def test_exec_raw(self):
        response = nqc.exec_raw(1)
        if response != b"":
            assert response == b""

        exp_args = ["nqc", "-Susb:/dev/usb/legousbtower0", "-raw", "1"]

        _nqc.subprocess.check_output.assert_called_with(exp_args, stderr=-1)


class DUTBase:
    @classmethod
    def get_nqc_obj(cls):
        return nqc

    @classmethod
    def get_rf_obj(cls):
        return rf

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        path = "./firmware/firm0309.lgo"
        exp_version = 3.9
        response = cls.get_rf_obj().get_versions()
        if not exp_version == float(response["desc-ram"]):
            response = cls.get_nqc_obj().flash_firmware(path)
        if not response:
            assert response, "Teardown flashing failed"
        response = cls.get_rf_obj().get_versions()
        assert exp_version == float(response["desc-ram"])

    def test_flash_firmware(self):
        path = "./firmware/firm0328.lgo"
        exp_version = 3.52
        response = self.get_nqc_obj().flash_firmware(path)
        if not response:
            assert response
        response = self.get_rf_obj().get_versions()
        assert exp_version == float(response["desc-ram"])

    def test_flash_and_exec_application(self):
        ID_VAR = 0
        VERSION_VAR = 1
        EXP_ID = 253
        EXP_VERSION = 255
        path = "./tests/test.nqc"
        flags = ["-DOVERRIDE", f"-DID=2", f"-DVERSION=2"]
        exp_flags = ["-DOVERRIDE", f"-DID={EXP_ID}", f"-DVERSION={EXP_VERSION}"]
        response = self.get_nqc_obj().flash_and_exec_application(path, flags)
        if not response:
            assert response
        id_var = self.get_rf_obj().get_value(_rf.VARIABLE, ID_VAR)
        version_var = self.get_rf_obj().get_value(_rf.VARIABLE, VERSION_VAR)

        response = self.get_nqc_obj().flash_and_exec_application(path, exp_flags)
        new_id_var = self.get_rf_obj().get_value(_rf.VARIABLE, ID_VAR)
        new_version_var = self.get_rf_obj().get_value(_rf.VARIABLE, VERSION_VAR)
        assert (
            id_var == 2
            and version_var == 2
            and new_id_var == 253
            and new_version_var == 255
        )


@pytest.mark.skipif(not is_dut_available(rf), reason="HW not available")
class TestOnHwDUT(DUTBase):
    @classmethod
    def get_nqc_obj(cls):
        return nqc

    @classmethod
    def get_rf_obj(cls):
        return rf


@pytest.mark.skipif(
    not pytest.emu or not is_dut_available(emu_rf), reason="EMU not available"
)
class TestOnEmuDUT(DUTBase):
    @classmethod
    def teardown_class(cls):
        pass

    @classmethod
    def get_nqc_obj(cls):
        return emu_nqc

    @classmethod
    def get_rf_obj(cls):
        return emu_rf

    @pytest.mark.skipif(True, reason="EMU does not seem to support flashing firmware")
    def test_flash_firmware(self):
        pass
