"""Example usage of pynqc."""

import time

import pynqc
from pynqc import raw_functions as rf


def run_example():
    """Execute the example."""
    compilation_flags = ["-DOVERRIDE", f"-DVERSION={1}", f"-DID={189}"]

    # An nqc instance requires knowledge of where it can find the lego IR tower
    nqc = pynqc.Nqc(serial_type="usb", device="/dev/usb/legousbtower0")

    # Raw functions also requires access to the lego IR tower
    # Therefore the nqc module has utility function which provides
    # an initialized raw_functions object

    raw_func = nqc.get_raw_functions()

    try:
        _ = raw_func.is_alive()
    except IOError as err:
        if err.errno == 255:
            print(err.strerror)
    else:
        _ = nqc.flash_firmware("../firmware/firm0309.lgo")
        _ = nqc.flash_and_exec_application("../tests/test.nqc", compilation_flags)
        time.sleep(1)
        raw_func.set_motor_state(rf.MOTOR_A, rf.MOTOR_ON)
        time.sleep(1)
        raw_func.set_motor_state(rf.MOTOR_A, rf.MOTOR_OFF)


if __name__ == "__main__":
    run_example()
