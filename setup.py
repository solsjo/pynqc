"""Setup script for pynqc"""
import distutils
import glob
import os
import shutil
from setuptools import setup


class CleanCommand(distutils.cmd.Command):
    """Custom clean command to tidy up the project root."""

    CLEAN_FILES = "./build ./dist ./*.pyc ./*.tgz ./*.egg-info".split(" ")

    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        here = os.path.dirname(os.path.abspath(__file__))

        for path_spec in self.CLEAN_FILES:
            # Make paths absolute and relative to this path
            abs_paths = glob.glob(os.path.normpath(os.path.join(here, path_spec)))
            for path in [str(p) for p in abs_paths]:
                if not path.startswith(here):
                    # Die if path in CLEAN_FILES is absolute + outside this
                    # directory
                    raise ValueError("%s is not a path inside %s" % (path, here))
                print("removing %s" % os.path.relpath(path))
                shutil.rmtree(path)


setup(
    name="pynqc",
    version="0.1",
    author="Oskar Solsjö",
    author_email="oskar.solsjo@gmail.com",
    url="https://gitlab.com/solsjo/pynqc",
    license="MIT",
    description="Wrapper for the nqc executable",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    packages=["pynqc"],
    zip_safe=False,
    python_requires=">=3.6",
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Operating System :: POSIX :: Linux",
    ],
    cmdclass={"clean": CleanCommand},
)
